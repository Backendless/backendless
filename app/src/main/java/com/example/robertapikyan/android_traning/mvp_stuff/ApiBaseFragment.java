package com.example.robertapikyan.android_traning.mvp_stuff;

import com.hannesdorfmann.mosby.mvp.MvpFragment;

/**
 * Created by RobertApikyan on 7/1/2016.
 */
public abstract class ApiBaseFragment<V extends ApiBaseView,P extends ApiBasePresenter<V>> extends MvpFragment<V,P> {

}
