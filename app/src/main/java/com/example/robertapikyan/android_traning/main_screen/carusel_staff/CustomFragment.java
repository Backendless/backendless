package com.example.robertapikyan.android_traning.main_screen.carusel_staff;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.robertapikyan.android_traning.R;

/**
 * Created by RobertApikyan on 7/21/2016.
 */
public class CustomFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.caruserl_fragment,container,false);
        return rootView;
    }
}
