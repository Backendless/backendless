package com.example.robertapikyan.android_traning.main_screen.carusel_staff;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.robertapikyan.android_traning.R;

import java.util.ArrayList;
import java.util.List;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

/**
 * Created by RobertApikyan on 7/21/2016.
 */
public class Main extends AppCompatActivity{
    ViewPager viewPager;
    FeatureCoverFlow mCoverFlow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        mCoverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        mCoverFlow.setAdapter(new MyAdapter(this));
        mCoverFlow.setShouldRepeat(true);
        mCoverFlow.setRadius(0.3f);
        mCoverFlow.setReflectionOpacity(0);
        mCoverFlow.setRotationTreshold(1);
        mCoverFlow.setMaxRotationAngle(350);
        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //TODO CoverFlow item clicked
            }
        });

        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                //TODO CoverFlow stopped to position
            }

            @Override
            public void onScrolling() {
                //TODO CoverFlow began scrolling
            }
        });
    }

    class MyAdapter extends BaseAdapter{
        List<ImageView> items = new ArrayList<>();
        LayoutInflater inflater;
        MyAdapter(Context context){
            inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = inflater.inflate(R.layout.m_image_view,parent,false);
            return rootView;
        }
    }

}
