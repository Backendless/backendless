package com.example.robertapikyan.android_traning.mvp_stuff;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.example.robertapikyan.android_traning.functional.Functions;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public abstract class ApiBaseActivity<V extends ApiBaseView,P extends ApiBasePresenter<V>> extends MvpActivity<V,P> implements ApiBaseView{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onCreate();

        presenter.executeFunction(Functions.GET_LOCALES, new ModelCallBack<String>() {
            @Override
            public void call(String s) {
                Log.d("Loog" , s);
            }
        });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String errorMessage) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(errorMessage);
        builder.setCancelable(false);
        builder.setTitle("ERROR");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }
}
