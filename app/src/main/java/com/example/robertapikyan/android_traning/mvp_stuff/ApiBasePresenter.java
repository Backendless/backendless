package com.example.robertapikyan.android_traning.mvp_stuff;

import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.example.robertapikyan.android_traning.functional.FunctionManager;
import com.example.robertapikyan.android_traning.functional.Functions;
import com.example.robertapikyan.android_traning.loggin.L;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public abstract class ApiBasePresenter<V extends ApiBaseView> extends MvpBasePresenter<V> implements AsyncCallback{
    public void onCreate() {
        ApiBaseModel.register();
    }

    public void executeFunction(Functions function,ModelCallBack<?> modelCallBack) {
        FunctionManager.execute(function, modelCallBack);
    }

    public void executeAction(Functions function) {
        FunctionManager.execute(function, null);
    }

    @Override
    public void handleFault(BackendlessFault fault) {

        L.Log(fault.getMessage());
        L.Log(fault.getCode());
        L.Log(fault.getDetail());

       if (isViewAttached())getView().showError(fault.getMessage());
    }

}
