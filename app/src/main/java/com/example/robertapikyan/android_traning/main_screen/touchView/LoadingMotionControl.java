package com.example.robertapikyan.android_traning.main_screen.touchView;

/**
 * Created by RobertApikyan on 7/12/2016.
 */
interface LoadingMotionControl {
    void addRadius(int position,float... oldRadius);

    void subtractRadius(int position,float... oldRadius);
}
