package com.example.robertapikyan.android_traning.mvp_stuff;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public class ApiBaseModel{
    private static ApiBaseModel model;
    public static void register(){
        model = new ApiBaseModel();
    }

    public static void unregister(){
        model = null;
    }

//    private ApiBaseModel(){
//        new FunctionManager.FunctionBuilder().add(new Function() {
//            @Override
//            public void call(ModelCallBack modelCallBack) {
//                String callback = "Functions.GET_LOCALES";
//                if (modelCallBack!=null) {
//                    modelCallBack.call(callback);
//                }
//            }
//
//            @Override
//            public Functions getKey() {
//                return Functions.GET_LOCALES;
//            }
//        }).add(new Function() {
//            @Override
//            public void call(ModelCallBack modelCallBack) {
//                modelCallBack.call("ok");
//            }
//
//            @Override
//            public Functions getKey() {
//                return Functions.GET_WEATHER;
//            }
//        }).register();
//    }

}
