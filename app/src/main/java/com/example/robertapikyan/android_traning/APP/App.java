package com.example.robertapikyan.android_traning.APP;

import android.app.Application;

import com.backendless.Backendless;
import com.example.robertapikyan.android_traning.backendless_stuff.BackendlessKeyStore;
import com.example.robertapikyan.android_traning.loggin.L;
import com.facebook.FacebookSdk;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public class App extends Application{
    private static App ourInstance = new App();

    public static App getInstance() {
        return ourInstance;
    }

    public App() {
        super();
    }


    @Override
    public void onCreate() {
        ourInstance = this;
        super.onCreate();
        initBackendless();
        initFB();
    }

    private void initBackendless(){
        Backendless.initApp(getApplicationContext(), BackendlessKeyStore.APP_ID, BackendlessKeyStore.APP_SECRET_KEY, BackendlessKeyStore.APP_VERSION);
        L.Log("Backendless init");
    }

    private void initFB(){
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
