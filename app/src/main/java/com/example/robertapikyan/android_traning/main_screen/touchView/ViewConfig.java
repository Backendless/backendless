package com.example.robertapikyan.android_traning.main_screen.touchView;

/**
 * Created by RobertApikyan on 7/12/2016.
 */
public class ViewConfig {
    private int startColor, endColor;
    private int cyclesCount;
    private int FPS;
    private float[] maxRadius;
    private float[] minRadius;
    private float[] radius;

    public Builder newBuilder() {
        return new Builder();
    }

    public int getStartColor() {
        return startColor;
    }

    public int getEndColor() {
        return endColor;
    }

    public int getCyclesCount() {
        return cyclesCount;
    }

    public int getFPS() {
        return FPS;
    }

    public float[] getMaxRadius() {
        return maxRadius;
    }

    public float[] getMinRadius() {
        return minRadius;
    }

    public float[] getRadius() {
        return radius;
    }

    public class Builder {
        private Builder() {
        }

        public Builder setColor(int... color) {
            if (color.length >= 2) {
                ViewConfig.this.startColor = color[0];
                ViewConfig.this.endColor = color[1];
            }
            return this;
        }

        public Builder setStartColor(int color) {
            ViewConfig.this.startColor = color;
            return this;
        }

        public Builder setEndColor(int color) {
            ViewConfig.this.endColor = color;
            return this;
        }

        public Builder setCyclesCount(int count) {
            ViewConfig.this.cyclesCount = count;
            return this;
        }

        public Builder setFPS(int fps) {
            ViewConfig.this.FPS = fps;
            return this;
        }

        public Builder setMaxRadius(float[] radius) {
            ViewConfig.this.maxRadius = radius;
            return this;
        }

        public Builder setMinRadius(float[] radius) {
            ViewConfig.this.minRadius = radius;
            return this;
        }

        public Builder setRadius(float[] radius) {
            ViewConfig.this.radius = radius;
            return this;
        }

        public Builder changeType(ChangeType changeType) {
            switch (changeType) {
                case DEF:
                    //TODO
                    break;
                case SIN:
                    //TODO
                    break;
            }
            return this;
        }

        public ViewConfig build() {
            return ViewConfig.this;
        }
    }

    public enum ChangeType {
        SIN,
        DEF;
    }
}
