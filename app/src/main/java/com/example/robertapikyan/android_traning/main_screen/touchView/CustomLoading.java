package com.example.robertapikyan.android_traning.main_screen.touchView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by RobertApikyan on 7/8/2016.
 */
public class CustomLoading extends View implements LoadingMotionControl, LoadingOnMeasure {
    private int mViewHeight, mViewWith;
    private int[] mDrawHeight, mDrawWith, mDrawWithDelay, mDrawHeightDelay;
    private Paint[] mPaint;
    private boolean[] mRadiusIncrement = {false,false,false,false,false};
    private boolean[] mRadiusDecrement = {true,true,true,true,true};
    private long prevTime;
    private long nowTime;
    private long mRadiusGrowSpeed = 1;
    private LoadingMotionControl mMotionControl = this;
    private ViewConfig mViewConfig = new ViewConfig().newBuilder()
            .setColor(43654164, 51313131)
            .setFPS(48)
            .setMaxRadius(new float[]{60, 65, 70, 65,60})
            .setRadius(new float[]{22, 32, 42, 32, 22})
            .setMinRadius(new float[]{20, 15, 20, 15, 20})
            .setCyclesCount(5).build();

    private LoadingOnMeasure mLoadingOnMeasure = this;

    public void setLoadingMotionControl(LoadingMotionControl control) {
        if (control != null) this.mMotionControl = control;
    }

    public void setViewConfig(ViewConfig config) {
        if (config != null) this.mViewConfig = config;
    }

    public void setOnMeasureHandler(LoadingOnMeasure measure) {
        if (measure != null) mLoadingOnMeasure = measure;
    }

    public CustomLoading(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        initPaints();
        initTime();
    }

    public ViewConfig getmViewConfig(){
        return mViewConfig;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mDrawHeight = new int[mViewConfig.getCyclesCount()];
        mDrawWith = new int[mViewConfig.getCyclesCount()];
        mDrawWithDelay = new int[mViewConfig.getCyclesCount()];
        mDrawHeightDelay = new int[mViewConfig.getCyclesCount()];

        mLoadingOnMeasure.onMeasure();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (canChange()) {
            initRadius();
            initPaints();
        }
            drawCanvas(canvas);
        invalidate();
    }

    private void drawCanvas(Canvas canvas) {
        for (int i = 0; i < mViewConfig.getCyclesCount(); i++) {
            canvas.drawCircle(mDrawWith[i], mDrawHeight[i], mViewConfig.getRadius()[i], mPaint[i]);
        }
    }

    private void initMotion(int i) {

        if (mViewConfig.getRadius()[i] >= mViewConfig.getMaxRadius()[i]) {
            mRadiusDecrement[i] = true;
            mRadiusIncrement[i] = false;
        }

        if (mViewConfig.getRadius()[i] <= mViewConfig.getMinRadius()[i]) {
            mRadiusDecrement[i] = false;
            mRadiusIncrement[i] = true;
        }
    }

    private void initRadius() {
            for (int i = 0; i < mViewConfig.getCyclesCount(); i++) {
                initMotion(i);
                if (mRadiusIncrement[i]) {
                    mMotionControl.addRadius(i,mViewConfig.getRadius());
                } else {
                    if (mRadiusDecrement[i]) {
                        mMotionControl.subtractRadius(i,mViewConfig.getRadius());
                    }
                }
        }
    }

    private boolean canChange() {
        nowTime =System.currentTimeMillis();
        if (nowTime - prevTime >= 1000 / mViewConfig.getFPS()) {
            prevTime = nowTime;
            return true;
        }
        return false;
    }

    private void initTime() {
        prevTime = System.currentTimeMillis();
    }

    private void initPaints() {
        mPaint = new Paint[mViewConfig.getCyclesCount()];
        int color = mViewConfig.getStartColor() - mViewConfig.getEndColor();
        for (int i = 1; i <= mViewConfig.getCyclesCount(); i++) {
            mPaint[i - 1] = new Paint();
            mPaint[i - 1].setColor(color / i);
        }
    }

    @Override
    public void addRadius(int position,float... oldRadius) {
        mViewConfig.getRadius()[position] = oldRadius[position] + mRadiusGrowSpeed;
    }

    @Override
    public void subtractRadius(int position,float... oldRadius) {
        mViewConfig.getRadius()[position] = oldRadius[position] - mRadiusGrowSpeed;
    }


    @Override
    public void onMeasure() {
        mViewHeight = getLayoutParams().height;
        mViewWith = getLayoutParams().width;

        for (int i = 0; i < mViewConfig.getCyclesCount(); i++) {
            int withDelay = mViewWith / (mViewConfig.getCyclesCount() + 1);
            mDrawHeight[i] = mViewHeight / 2;
            mDrawWith[i] = withDelay + i * withDelay;
        }
    }
}

