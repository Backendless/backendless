package com.example.robertapikyan.android_traning.functional;

import com.example.robertapikyan.android_traning.mvp_stuff.ModelCallBack;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public  interface  Function<K extends Functions> extends Key<K> {
     void call(ModelCallBack<?> modelCallBack);
}
