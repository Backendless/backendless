package com.example.robertapikyan.android_traning.main_screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.backendless.Backendless;
import com.example.robertapikyan.android_traning.R;
import com.example.robertapikyan.android_traning.loggin.L;
import com.example.robertapikyan.android_traning.mvp_stuff.ApiBaseActivity;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends ApiBaseActivity<MainScreenView,MainScreenPresenter> implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private EditText userName,password;
    private LoginButton fbB;
    CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private static final int PLACE_PICKER_REQUEST = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userName = (EditText) findViewById(R.id.user);
        password = (EditText) findViewById(R.id.password);


        callbackManager = CallbackManager.Factory.create();


        mGoogleApiClient = new GoogleApiClient
                .Builder( this )
                .enableAutoManage( this, 0, this )
                .addApi( Places.GEO_DATA_API )
                .addApi( Places.PLACE_DETECTION_API )
                .addConnectionCallbacks( this )
                .addOnConnectionFailedListener( this )
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
        if( requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK ) {
            L.ToastLong(PlacePicker.getPlace( data, this ).getAddress().toString(),this);
        }
    }

    @NonNull
    @Override
    public MainScreenPresenter createPresenter() {
        return new MainScreenPresenter();
    }

    public void login(View view){
       presenter.login(userName.getText().toString(), password.getText().toString());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if( mGoogleApiClient != null )
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    public void displayPlacePicker(View view) {
        if( mGoogleApiClient == null || !mGoogleApiClient.isConnected() )
            return;

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult( builder.build( getApplicationContext() ), PLACE_PICKER_REQUEST );
        } catch ( GooglePlayServicesRepairableException e ) {
            Log.d("PlacesAPI Demo", "GooglePlayServicesRepairableException thrown");
        } catch ( GooglePlayServicesNotAvailableException e ) {
            Log.d( "PlacesAPI Demo", "GooglePlayServicesNotAvailableException thrown" );
        }
    }

    public void loginFB(View view){
        Map<String, String> facebookFieldMappings = new HashMap<String, String>();
        facebookFieldMappings.put( "email", "fb_email" );
        facebookFieldMappings.put("name" ,"name");

        List<String> permissions = new ArrayList<>();
        permissions.add( "email" );
        Backendless.UserService.loginWithFacebookSdk(this,facebookFieldMappings,permissions,callbackManager,presenter);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        L.Log(String.valueOf(connectionResult.isSuccess()));
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
