package com.example.robertapikyan.android_traning.backendless_stuff;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

public class Notes
{
  private String note;
  private java.util.Date created;
  private String objectId;
  private String ownerId;
  private java.util.Date updated;
  public String getNote()
  {
    return note;
  }

  public void setNote( String note )
  {
    this.note = note;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

                                                    
  public Notes save()
  {
    return Backendless.Data.of( Notes.class ).save( this );
  }

  public Future<Notes> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Notes> future = new Future<Notes>();
      Backendless.Data.of( Notes.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Notes> callback )
  {
    Backendless.Data.of( Notes.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Notes.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Notes.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Notes.class ).remove( this, callback );
  }

  public static Notes findById( String id )
  {
    return Backendless.Data.of( Notes.class ).findById( id );
  }

  public static Future<Notes> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Notes> future = new Future<Notes>();
      Backendless.Data.of( Notes.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Notes> callback )
  {
    Backendless.Data.of( Notes.class ).findById( id, callback );
  }

  public static Notes findFirst()
  {
    return Backendless.Data.of( Notes.class ).findFirst();
  }

  public static Future<Notes> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Notes> future = new Future<Notes>();
      Backendless.Data.of( Notes.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Notes> callback )
  {
    Backendless.Data.of( Notes.class ).findFirst( callback );
  }

  public static Notes findLast()
  {
    return Backendless.Data.of( Notes.class ).findLast();
  }

  public static Future<Notes> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Notes> future = new Future<Notes>();
      Backendless.Data.of( Notes.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Notes> callback )
  {
    Backendless.Data.of( Notes.class ).findLast( callback );
  }

  public static BackendlessCollection<Notes> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Notes.class ).find( query );
  }

  public static Future<BackendlessCollection<Notes>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Notes>> future = new Future<BackendlessCollection<Notes>>();
      Backendless.Data.of( Notes.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Notes>> callback )
  {
    Backendless.Data.of( Notes.class ).find( query, callback );
  }
}