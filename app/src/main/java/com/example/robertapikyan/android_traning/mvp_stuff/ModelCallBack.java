package com.example.robertapikyan.android_traning.mvp_stuff;

/**
 * Created by RobertApikyan on 7/6/2016.
 */
public interface ModelCallBack<T> {
    public void call(T t);
}
