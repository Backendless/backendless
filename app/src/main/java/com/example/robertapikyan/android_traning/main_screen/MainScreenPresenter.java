package com.example.robertapikyan.android_traning.main_screen;

import android.content.Context;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.property.UserProperty;
import com.example.robertapikyan.android_traning.loggin.L;
import com.example.robertapikyan.android_traning.mvp_stuff.ApiBasePresenter;

import java.util.List;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public class MainScreenPresenter extends ApiBasePresenter<MainScreenView>{

    @Override
    public void handleResponse(Object response) {
        BackendlessUser user = (BackendlessUser) response;
        Backendless.UserService.describeUserClass(new AsyncCallback<List<UserProperty>>() {
            @Override
            public void handleResponse(List<UserProperty> response) {
                response.size();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                getView().showError(fault.getMessage());
            }
        });
        L.Log(user.getEmail());
    }

    public void login(String user,String password){
        Backendless.UserService.login(user, password, this);
    }

    public void loginFB(Context context){

    }

}
