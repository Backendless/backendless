package com.example.robertapikyan.android_traning.main_screen;

import com.example.robertapikyan.android_traning.mvp_stuff.ApiBaseView;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public interface MainScreenView extends ApiBaseView {
    public void showSusses(String sussesMessage);
}
