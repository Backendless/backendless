package com.example.robertapikyan.android_traning.functional;

/**
 * Created by RobertApikyan on 7/6/2016.
 */
public enum Functions {
    GET_WEATHER,
    GET_LOCALES
}
