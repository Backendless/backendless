package com.example.robertapikyan.android_traning.Helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by RobertApikyan on 7/1/2016.
 */
public class FragmentHelpers {

    public static void addFragment(FragmentManager manager,Fragment fragment,int containerViewId,String fragmentTag){
        FragmentTransaction transaction  = manager.beginTransaction();
        transaction.add(containerViewId,fragment,fragmentTag);
        transaction.commit();
    }

    public static void replaceFragment(FragmentManager manager,Fragment fragment, int containerViewId , String fragmentTag){
        FragmentTransaction transaction  = manager.beginTransaction();
        transaction.replace(containerViewId, fragment, fragmentTag);
        transaction.commit();
    }

    public static void removeFragment(FragmentManager manager,String fragmentTag){
        FragmentTransaction transaction  = manager.beginTransaction();
        Fragment fragment = manager.findFragmentByTag(fragmentTag);
        transaction.remove(fragment);
    }

}
