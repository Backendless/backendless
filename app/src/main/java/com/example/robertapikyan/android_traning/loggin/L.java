package com.example.robertapikyan.android_traning.loggin;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by RobertApikyan on 7/19/2016.
 */
public class L {
    private static String logKey = "Loog";

    public static void Log(String msg) {
        if (msg!=null && msg.length()!=0)Log.d(logKey, msg);
    }

    public static void ToastLong(String msg,Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void ToastShort(String msg,Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public void setLogKey(String logKey) {
        this.logKey = logKey;
    }

}
