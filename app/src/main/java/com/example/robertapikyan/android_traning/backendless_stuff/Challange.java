package com.example.robertapikyan.android_traning.backendless_stuff;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

public class Challange
{
  private String objectId;
  private String message;
  private String title;
  private String ownerId;
  private java.util.Date created;
  private java.util.Date updated;
  public String getObjectId()
  {
    return objectId;
  }

  public String getMessage()
  {
    return message;
  }

  public void setMessage( String message )
  {
    this.message = message;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle( String title )
  {
    this.title = title;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

                                                    
  public Challange save()
  {
    return Backendless.Data.of( Challange.class ).save( this );
  }

  public Future<Challange> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Challange> future = new Future<Challange>();
      Backendless.Data.of( Challange.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Challange> callback )
  {
    Backendless.Data.of( Challange.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Challange.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Challange.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Challange.class ).remove( this, callback );
  }

  public static Challange findById( String id )
  {
    return Backendless.Data.of( Challange.class ).findById( id );
  }

  public static Future<Challange> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Challange> future = new Future<Challange>();
      Backendless.Data.of( Challange.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Challange> callback )
  {
    Backendless.Data.of( Challange.class ).findById( id, callback );
  }

  public static Challange findFirst()
  {
    return Backendless.Data.of( Challange.class ).findFirst();
  }

  public static Future<Challange> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Challange> future = new Future<Challange>();
      Backendless.Data.of( Challange.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Challange> callback )
  {
    Backendless.Data.of( Challange.class ).findFirst( callback );
  }

  public static Challange findLast()
  {
    return Backendless.Data.of( Challange.class ).findLast();
  }

  public static Future<Challange> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Challange> future = new Future<Challange>();
      Backendless.Data.of( Challange.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Challange> callback )
  {
    Backendless.Data.of( Challange.class ).findLast( callback );
  }

  public static BackendlessCollection<Challange> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Challange.class ).find( query );
  }

  public static Future<BackendlessCollection<Challange>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Challange>> future = new Future<BackendlessCollection<Challange>>();
      Backendless.Data.of( Challange.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Challange>> callback )
  {
    Backendless.Data.of( Challange.class ).find( query, callback );
  }
}