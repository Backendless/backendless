package com.example.robertapikyan.android_traning.functional;

import com.example.robertapikyan.android_traning.functional.Functions;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public interface Key<K extends Functions> {
    K getKey();
}
