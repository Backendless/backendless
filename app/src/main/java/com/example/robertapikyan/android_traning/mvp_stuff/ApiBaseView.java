package com.example.robertapikyan.android_traning.mvp_stuff;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by RobertApikyan on 6/30/2016.
 */
public interface ApiBaseView extends MvpView {
        void showLoading();
        void hideLoading();
        void showError(String errorMessage);
}
