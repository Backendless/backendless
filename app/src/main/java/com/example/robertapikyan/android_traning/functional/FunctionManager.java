package com.example.robertapikyan.android_traning.functional;

import com.example.robertapikyan.android_traning.mvp_stuff.ModelCallBack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by RobertApikyan on 6/30/2016.
 */
public class FunctionManager {
    private static List<Function> functions = new ArrayList<>();
    private static FunctionManager instance = new FunctionManager();

    public static FunctionManager getInstance() {
        return instance;
    }

    private FunctionManager(){}

    public static void execute(Functions key, ModelCallBack<?> callBack) {
        Iterator<Function> iterator = functions.iterator();
        while (iterator.hasNext()) {
            Function function = iterator.next();
            if (function.getKey() == key) {
                function.call(callBack);
            }
        }
    }

    /**
     * Created by RobertApikyan on 6/30/2016.
     */
    public static class FunctionBuilder {
        private List<Function> f;

        public FunctionBuilder() {
            f = new ArrayList<>();
        }

        public FunctionBuilder add(Function function) {
            f.add(function);
            return this;
        }


        public void register() {
            functions.addAll(f);
        }

        public void addAndRegister(Function function) {
            functions.add(function);
        }
    }

}
